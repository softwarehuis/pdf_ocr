# Requires tesseract and at least tesseract-data-eng, please install it for your linux distro
pytesseract==0.3.10
pdf2image==1.16.0
Pillow~=9.2.0
borb==2.1.4
lxml==4.9.1
