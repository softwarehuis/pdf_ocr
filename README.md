# Installatie
- Installeer [tesseract](https://tesseract-ocr.github.io/tessdoc/Home.html) en tenminste de **engelse vertaal data** voor je linux distributie
- Installeer de benodigde python afhankelijkheden met:

```python
pip3 install -r requirements.txt
```

# Test
Voer het volgende commando uit

```python
python3 pdf_ocr.py ./data
```



Je dient als het goed is in de data directory twee nieuwe bestanden terug te vinden te weten:
- sv600_c_automatic.ocr.pdf
- sv600_c_automatic.ocr.txt

Als je de ocr.pdf opent, dan is de tekst op de afbeelding nu te selecteren.
