#!/usr/bin/env python
import sys
from datetime import datetime
from enum import Enum
from pathlib import Path
from typing import Optional

from borb.pdf import Document, PDF, Page
from borb.pdf.page.page_info import PageInfo
from borb.pdf.trailer.document_info import DocumentInfo
from borb.toolkit.ocr.ocr_as_optional_content_group import OCRAsOptionalContentGroup
from borb.toolkit.text.simple_text_extraction import SimpleTextExtraction

import argparse


class ContentType(Enum):
    TEXT = "text"
    IMAGE = "image"


def init_argparse() -> argparse.ArgumentParser:
    """ Create argument parser """
    _parser = argparse.ArgumentParser(
        usage="%(prog)s [OPTION] [DIR] [TARGET_DIR*]",
        description="Process pdf-files from DIR and OCR them when needed. Write results to TARGET_DIR (* or DIR when "
                    "TARGET_DIR is not set)"
    )

    _parser.add_argument(
        "-v", "--version", action="version",
        version=f"{_parser.prog} version 1.0.0"
    )

    _parser.add_argument('dir', type=Path, nargs='?')
    _parser.add_argument('target_dir', type=Path, nargs='?')
    return _parser


def to_datetime(pdf_date):

    if pdf_date is None:
        return None
    try:
        return datetime.strptime(str(pdf_date).replace("'", ""), "D:%Y%m%d%H%M%S%z")
    except ValueError:
        return datetime.strptime(str(pdf_date).replace("'", ""), "D:%Y%m%d%H%M%S")


def to_int_exp(decimal_instance):
    """ Turn page numbers (which are decimals) to integers """
    list_d = str(decimal_instance).split('.')

    if len(list_d) == 2:
        # Negative exponent
        # exponent = -len(list_d[1])
        integer = int(list_d[0] + list_d[1])

    else:
        str_dec = list_d[0].rstrip('0')
        # Positive exponent
        # exponent = len(list_d[0]) - len(str_dec)
        try:
            integer = int(str_dec)
        except ValueError:
            return 0

    return integer


def create_text_file(file: Path, target: Path):
    """ Read a pdf document and generate a corresponding text file
    with the same name but with the .txt extension"""
    l: SimpleTextExtraction = SimpleTextExtraction()
    with open(file, "rb") as pdf_file_handle:
        doc = PDF.loads(pdf_file_handle, [l])

    info: DocumentInfo = doc.get_document_info()
    pages = info.get_number_of_pages()
    out_file = Path(target / (file.stem + ".txt"))
    with open(out_file, "w") as text_out:
        for x in range(to_int_exp(pages)):
            text_out.write(l.get_text_for_page(x))


def has(file: Path, term: ContentType) -> bool:
    """ Check if a pdf has a text layer, if so return true """
    with open(file, "rb") as pdf_file_handle:
        doc = PDF.loads(pdf_file_handle)
    contents = {
        "image": 0,
        "text": 0
    }

    info: DocumentInfo = doc.get_document_info()
    print(f" - title:       {info.get_title()}")
    print(f" - subject:     {info.get_subject()}")
    print(f" - creator:     {info.get_creator()}")
    print(f" - author:      {info.get_author()}")
    print(f" - created at:  {to_datetime(info.get_creation_date())}")
    print(f" - modified at: {to_datetime(info.get_modification_date())}")
    print(f" - pages:       {info.get_number_of_pages()}")
    for x in range(to_int_exp(info.get_number_of_pages())):
        p: Page = doc.get_page(x)
        pinfo: PageInfo = p.get_page_info()
        if pinfo.uses_text():
            contents["text"] = contents["text"] + 1
        if pinfo.uses_color_images() or pinfo.uses_grayscale_images() or pinfo.uses_indexed_images():
            contents["image"] = contents["image"] + 1

    if contents[term.value] > 0:
        return True
    return False


def apply_ocr_to_document(file: Path, target: Path):
    """ Perform ocr using tesseract to a pdf document """
    # Set up everything for OCR
    tesseract_data_dir: Path = Path("./tesseract-data/")
    assert tesseract_data_dir.exists()
    l: OCRAsOptionalContentGroup = OCRAsOptionalContentGroup(tesseract_data_dir)
    if file.suffix.lower() != ".pdf":
        raise ValueError("Not a pdf")

    # Read Document
    doc: Optional[Document] = None
    with open(file, "rb") as pdf_in:
        doc = PDF.loads(pdf_in, [l])

    assert doc is not None
    out_file = Path(target / (file.stem + ".ocr.pdf"))
    # Store Document as `ocr.pdf`
    with open(out_file, "wb") as pdf_out:
        PDF.dumps(pdf_out, doc)
    return out_file


if __name__ == '__main__':
    """ Main function """
    parser = init_argparse()

    args = parser.parse_args()
    if args.dir is None:
        parser.print_help(sys.stderr)
        sys.exit(1)
    if args.target_dir is None:
        # Resolve to source directory if no target directory is given.
        args.target_dir = args.dir
    print(f"TARGET_DIR:     {str(args.target_dir)}")

    try:
        files = args.dir.glob("*.pdf")
        for _file in files:
            if not _file.name.endswith(".ocr.pdf"):
                print(f"PROCESSING:     {str(_file)}")
                try:
                    if has(Path(_file), ContentType.IMAGE):
                        _file = apply_ocr_to_document(Path(_file), args.target_dir)
                except (KeyError, ValueError):
                    print(f"OCR FAILED for: {str(_file)}")
                create_text_file(_file, args.target_dir)
    except AttributeError:
        parser.print_help(sys.stderr)
        sys.exit(1)
